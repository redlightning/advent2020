#!/usr/bin/perl
use warnings;
use strict;
use Scalar::Util;

my $byr = "";
my $iyr = "";
my $eyr = "";
my $hgt = "";
my $hcl = "";
my $ecl = "";
my $pid = "";
my $cid = "";

my $count = 1;
my $filename = 'input.txt';

open(FH, '<', $filename) or die $!;

while(<FH>) {
    if ( /^$/ ) {
        if ( $byr ne "" && $iyr ne "" && $eyr ne "" && $hgt ne "" && $hcl ne "" && $ecl ne "" && $pid ne "" ) {
            $count = $count + 1;
        } 
        # Reset
        $byr = $iyr = $eyr = $hgt = $hcl = $ecl = $pid = $cid = "";
    } else {
        my @line = split(' ', $_);
        foreach my $dataset ( @line ) {
            my @tokens = split(':', $dataset);
            if ( $tokens[0] eq "byr" && $tokens[1] ~~ [1920..2002]) { $byr = $tokens[1]; }
            elsif ( $tokens[0] eq "iyr" && $tokens[1] ~~ [2010..2020]) { $iyr = $tokens[1]; }
            elsif ( $tokens[0] eq "eyr" && $tokens[1] ~~ [2020..2030]) { $eyr = $tokens[1]; }
            elsif ( $tokens[0] eq "hgt") { 
                if(index($tokens[1], "in") ne -1) {
                    my $cleaned = $tokens[1];
                    $cleaned =~ s/in//;
                    if($cleaned ~~ [59..76]) { $hgt = $tokens[1]; }
                } elsif(index($tokens[1], "cm") ne -1) {
                    my $cleaned = $tokens[1];
                    $cleaned =~ s/cm//;
                    if($cleaned ~~ [150..193]) { $hgt = $tokens[1]; }
                }
            } elsif ( $tokens[0] eq "hcl" && $tokens[1] =~ /[#]([0-9a-f]){6}/ ) { 
                $hcl = $tokens[1]; 
                print("$hcl\n")
            } elsif ( $tokens[0] eq "ecl" && ($tokens[1] eq "amb" || $tokens[1] eq "blu" || $tokens[1] eq "brn" || $tokens[1] eq "gry" || $tokens[1] eq "grn" || $tokens[1] eq "hzl" || $tokens[1] eq "oth")) { $ecl = $tokens[1]; }
            elsif ( $tokens[0] eq "pid" && length($tokens[1]) eq 9 && Scalar::Util::looks_like_number($tokens[1])) { $pid = $tokens[1]; }
            elsif ( $tokens[0] eq "cid") { $cid = $tokens[1]; }
        }
    }
}
close(FH);

print("$count\n");
