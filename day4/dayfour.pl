#!/usr/bin/perl
use warnings;
use strict;

my $byr = "";
my $iyr = "";
my $eyr = "";
my $hgt = "";
my $hcl = "";
my $ecl = "";
my $pid = "";
my $cid = "";

my $count = 1;
my $filename = 'input.txt';

open(FH, '<', $filename) or die $!;

while(<FH>) {
    if ( /^$/ ) {
        if ( $byr ne "" && $iyr ne "" && $eyr ne "" && $hgt ne "" && $hcl ne "" && $ecl ne "" && $pid ne "" ) {
            $count = $count + 1;
        } 
        # Reset
        $byr = $iyr = $eyr = $hgt = $hcl = $ecl = $pid = $cid = "";
    } else {
        my @line = split(' ', $_);
        foreach my $dataset ( @line ) {
            my @tokens = split(':', $dataset);
            if ( $tokens[0] eq "byr") { $byr = $tokens[1]; }
            elsif ( $tokens[0] eq "iyr") { $iyr = $tokens[1]; }
            elsif ( $tokens[0] eq "eyr") { $eyr = $tokens[1]; }
            elsif ( $tokens[0] eq "hgt") { $hgt = $tokens[1]; }
            elsif ( $tokens[0] eq "hcl") { $hcl = $tokens[1]; }
            elsif ( $tokens[0] eq "ecl") { $ecl = $tokens[1]; }
            elsif ( $tokens[0] eq "pid") { $pid = $tokens[1]; }
            elsif ( $tokens[0] eq "cid") { $cid = $tokens[1]; }
        }
    }
}
close(FH);

print("$count\n");
