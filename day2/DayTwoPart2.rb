#!/usr/bin/ruby

lines = IO.readlines("input.txt")
puts lines[0]
valid = 0

lines.each { |item| 
    fragments = item.split(":")
    rules = fragments[0]
    password = fragments[1].strip

    # Parse rules
    ruleParts = rules.split(" ")
    range = ruleParts[0].split("-")
    reqChar = ruleParts[1].strip

    first = password[range[0].to_i - 1]
    second = password[range[1].to_i - 1]

    if (first === reqChar) ^ (second === reqChar)
        puts item.strip + " " + (first === reqChar).to_s + ", " + (second === reqChar).to_s
        valid = valid + 1
    else 
        puts item.strip + " " + (first === reqChar).to_s + ", " + (second === reqChar).to_s + " X"
    end
}
puts "Part 2: " + valid.to_s
