#!/usr/bin/ruby

lines = IO.readlines("input.txt")
puts lines[0]
valid = 0

lines.each { |item| 
    fragments = item.split(":")
    rules = fragments[0]
    password = fragments[1].strip

    # Parse rules
    ruleParts = rules.split(" ")
    range = ruleParts[0].split("-")
    reqChar = ruleParts[1].strip

    count = password.count(reqChar)
    
    if count.to_i.between?(range[0].to_i, range[1].to_i) 
        puts item.strip + " " + reqChar + " " + range[0] + "<" + count.to_s + ">" + range[1]
        valid = valid + 1
    else
        puts item.strip + " " + reqChar + " " + range[0] + "<" + count.to_s + ">" + range[1] + " X"
    end
}
puts "Part 1: " + valid.to_s
