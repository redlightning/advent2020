package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	lines, err := readLines("input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	for i, line := range lines {
		fmt.Println(i, line)
	}
	fmt.Println(len(lines))

	var acc = 0
	var activeIndex = 0
	var executedArray = make([]int, len(lines))
	var running = true
	for running {
		if executedArray[activeIndex] == 0 {
			executedArray[activeIndex] = 1
			var command = lines[activeIndex]
			var parts = strings.Split(command, " ")
			if parts[0] == "nop" {
				activeIndex++
				fmt.Println(command, " -> ", activeIndex)
			} else if parts[0] == "acc" {
				inc, err := strconv.Atoi(parts[1])
				if err != nil {
					log.Fatalf("acc parsing int: %s -> %s", command, err)
				}
				acc = acc + inc
				activeIndex++
				fmt.Println(command, " -> ", acc)
			} else if parts[0] == "jmp" {
				inc, err := strconv.Atoi(parts[1])
				if err != nil {
					log.Fatalf("jmp parsing int: %s -> %s", command, err)
				}
				activeIndex = activeIndex + inc
				fmt.Println(command, " -> ", activeIndex)
			}
		} else {
			fmt.Println("ACC ", acc)
			running = false
		}
	}
}

// readLines reads a whole file into memory and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
