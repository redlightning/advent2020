package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func main() {
	start := time.Now()
	lines, err := readLines("../input.txt")
	if err != nil {
		log.Fatalf("readLines: %s", err)
	}
	for i, line := range lines {
		fmt.Println(i, line)
	}
	fmt.Println(len(lines))

	for linenum := range lines {
		var running = true
		var acc = 0
		var activeIndex = 0
		var executedArray = make([]int, len(lines))

		//Change Command
		if strings.HasPrefix(lines[linenum], "nop") {
			fmt.Printf("Changing line %d %s %s", linenum, ":", lines[linenum])
			lines[linenum] = strings.Replace(lines[linenum], "nop", "jmp", 1)
			fmt.Println(" to ", lines[linenum])
		} else if strings.HasPrefix(lines[linenum], "jmp") {
			fmt.Printf("Changing line %d %s %s", linenum, ":", lines[linenum])
			lines[linenum] = strings.Replace(lines[linenum], "jmp", "nop", 1)
			fmt.Println(" to ", lines[linenum])
		}
		//Only chech lines when we changed something
		if strings.HasPrefix(lines[linenum], "nop") || strings.HasPrefix(lines[linenum], "jmp") {
			for running {
				if activeIndex > len(executedArray)-1 {
					duration := time.Since(start)
					fmt.Println("FINAL ACC ", acc)
					fmt.Println("Dur: ", duration)
					os.Exit(0)
				}
				if executedArray[activeIndex] == 0 {
					executedArray[activeIndex] = 1
					var command = lines[activeIndex]
					if len(command) < 3 {
						fmt.Println("FINAL ACC ", acc)
						os.Exit(0)
					}
					var parts = strings.Split(command, " ")
					if parts[0] == "nop" {
						activeIndex++
						//fmt.Println(command, " -> ", activeIndex)
					} else if parts[0] == "acc" {
						inc, err := strconv.Atoi(parts[1])
						if err != nil {
							log.Fatalf("acc parsing int: %s -> %s", command, err)
						}
						acc = acc + inc
						activeIndex++
						//fmt.Println(command, " -> ", acc)
					} else if parts[0] == "jmp" {
						inc, err := strconv.Atoi(parts[1])
						if err != nil {
							log.Fatalf("jmp parsing int: %s -> %s", command, err)
						}
						activeIndex = activeIndex + inc
						//fmt.Println(command, " -> ", activeIndex)
					}
				} else {
					fmt.Println("ACC ", acc)
					running = false
				}
			}
		}
		//Change it back so we're ready for the next line
		if strings.HasPrefix(lines[linenum], "nop") {
			lines[linenum] = strings.Replace(lines[linenum], "nop", "jmp", 1)
		} else if strings.HasPrefix(lines[linenum], "jmp") {
			lines[linenum] = strings.Replace(lines[linenum], "jmp", "nop", 1)
		}
	}
}

// readLines reads a whole file into memory and returns a slice of its lines.
func readLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
