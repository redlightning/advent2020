import java.io.File
import kotlin.system.exitProcess
import java.lang.IndexOutOfBoundsException

fun main() {
    var contents = File("input.txt").useLines { it.toList() }
    //contents.forEach { line -> println(line) }

    println("Read " + contents.size + " rows")
    var rollingNums = mutableListOf<Long>()
    for (i in 0..24) {
        rollingNums.add(contents.get(i).toLong())
    }

    for (x in 25..contents.size) {
        try {
            var numberToCheck = contents.get(x).toLongOrNull()
            if (numberToCheck != null) {
                var foundMatch = false
                for (f in 0..23) {
                    for (s in 1..24) {
                        if(rollingNums.get(f) + rollingNums.get(s) == numberToCheck) {
                            foundMatch = true
                        }
                    }
                }

                if(foundMatch) {
                    rollingNums.removeFirst()
                    rollingNums.add(numberToCheck)
                } else {
                    println("Invalid: " + numberToCheck)
                    exitProcess(0)
                }
            }
        } catch (o_O: IndexOutOfBoundsException) {
            println("EOF")
        }
    }
}
