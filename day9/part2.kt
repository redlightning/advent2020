import java.io.File
import kotlin.system.exitProcess
import java.lang.IndexOutOfBoundsException

fun main() {
    //var invalid = findInvalidNumber(5, "sample.txt")
    var invalid = findInvalidNumber(25, "input.txt")
    println(invalid)
    //var sumBigSmall = findConsecutiveSum(invalid, "sample.txt")
    var sumBigSmall = findConsecutiveSum(invalid, "input.txt")
    println(sumBigSmall)
}

fun findConsecutiveSum(target: Long, filename: String): Long {
    var contents = File(filename).useLines { it.toList() }
    var startIndex = 1;
    var lowest = contents.get(0).toLong()
    var rollingTotal = lowest
    var biggest = 0L;
    while (true) {
        for (num in startIndex+1..contents.size) {
            println("\t" + rollingTotal + " + " + contents.get(num).toLong())
            rollingTotal = rollingTotal + contents.get(num).toLong()
            if (contents.get(num).toLong() > biggest) {
                biggest = contents.get(num).toLong()
            } else if (contents.get(num).toLong() < lowest) {
                lowest = contents.get(num).toLong()
            }

            if (rollingTotal == target) {
                return contents.get(startIndex).toLong() + biggest
            } else if (rollingTotal > target) {
                startIndex++
                println("" + rollingTotal + " after " + num + " rows. Restarting at line " + startIndex)
                if(startIndex == contents.size) {
                    println("well, shit")
                    exitProcess(1)
                }
                rollingTotal = contents.get(startIndex).toLong()
                break
            }
        }
    }
} 

fun findInvalidNumber(preamble: Int, filename: String): Long {
    //var preamble = 5 //25
    var contents = File(filename).useLines { it.toList() }
    //var invalid = 127 //776203571

    var rollingNums = mutableListOf<Long>()
    for (i in 0..preamble-1) {
        rollingNums.add(contents.get(i).toLong())
    }

    for (x in preamble..contents.size) {
        try {
            var numberToCheck = contents.get(x).toLongOrNull()
            if (numberToCheck != null) {
                var foundMatch = false
                for (f in 0..preamble-2) {
                    for (s in 1..preamble-1) {
                        if(rollingNums.get(f) + rollingNums.get(s) == numberToCheck) {
                            foundMatch = true
                        }
                    }
                }

                if(foundMatch) {
                    rollingNums.removeFirst()
                    rollingNums.add(numberToCheck)
                } else {
                    println("Invalid: " + numberToCheck)
                    return numberToCheck
                }
            }
        } catch (o_O: IndexOutOfBoundsException) {
            println("EOF")
        }
    }
    return 0L  
}
