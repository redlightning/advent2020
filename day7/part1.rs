use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::time::Instant;

fn main() {
    let now = Instant::now();

    let mut rules = Vec::new();
    let mut colors = Vec::new();
    let mut some_left = true;

    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(bag) = line {
                rules.push(bag);
            }
        }
    }
    println!("{}", rules.len());
    
    for row in rules.iter() {
        let split = row.split(" bags contain ");
        let vec = split.collect::<Vec<&str>>();
        if vec[1].contains("shiny gold") {
            colors.push(vec[0]);
        }
    }

    while some_left {
        some_left = false;
        let mut color_to_add = "";
        for color in colors.iter() {
            for row in rules.iter() {
                let split = row.split(" bags contain ");
                let vec = split.collect::<Vec<&str>>();
                if vec[1].contains(color) && !colors.contains(&vec[0]) {
                    color_to_add = vec[0];
                    some_left = true;
                    break;
                }
            }
            if color_to_add.len() > 0 {
                break;
            }
        }    
        if color_to_add.len() > 0 {
            colors.push(color_to_add);
            println!("Added {} ({})", color_to_add, colors.len())
        }    
    }
    println!("{}", colors.len());
    println!("{}", now.elapsed().as_secs());
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}