use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

fn main() {
    let mut rules = Vec::new();
    let mut colors: Vec<&str> = Vec::new();
    //let mut some_left = true;
    let mut total_count = 0;

    //"./input.txt"
    if let Ok(lines) = read_lines("./input.txt") {
        for line in lines {
            if let Ok(bag) = line {
                rules.push(bag);
            }
        }
    }
    println!("{}", rules.len());

    // for row in rules.iter() {
    //     let split = row.split(" bags contain ");
    //     let vec = split.collect::<Vec<&str>>();
    //     if vec[1].contains("shiny gold") {
    //         let split_bags = vec[1].split(",");
    //         let contained_bags = split_bags.collect::<Vec<&str>>();
    //         colors.extend(contained_bags);       
    //     }
    // }
    
    // Input
    colors.push("1 pale turquoise bag");
    
    // Sample
    //colors.push("1 dark olive bag");
    //colors.push("2 vibrant plum bags");
    
    // Sample 2
    //colors.push("2 dark red bags.");

    println!("Bags that can hold shiny gold: {}", colors.len());

    while !colors.is_empty() {
        let mut next_bags = Vec::new();
        
        colors.retain(|color| {
            let delete = {
                //Popped bag
                println!("{}", color.trim());
                let split_color = color.trim().splitn(2, " ");
                let bag_list = split_color.collect::<Vec<&str>>();

                //Add the count for popped bag
                println!("\tAdding {} from {}", bag_list[0], bag_list[1]);
                let count = bag_list[0].parse::<i32>();
                let mut to_add = 0;
                match count {
                    Ok(ok) => to_add = ok,
                    Err(_e) => to_add = 0,
                }

                total_count = total_count + to_add;

                // Clean up the name
                let cleaned = bag_list[1].replace(" bags", "").replace(" bag", "").replace(" bags.", "").replace(" bag.", "").replace(".", "");
                println!("\tCleaned: {}", cleaned);

                for row in rules.iter() {
                    let split = row.split(" bags contain ");
                    let vec = split.collect::<Vec<&str>>();
                    if vec[0].contains(&cleaned) {
                        if !vec[1].contains("no other") {      
                            println!("\tNext: {}", vec[1]);
                            let split_bags = vec[1].split(",");
                            let contained_bags = split_bags.collect::<Vec<&str>>();
                            //multiply each sub-bag in this bag
                            for _x in 0..to_add {
                                next_bags.extend(contained_bags.clone());
                            }
                        } else {
                            println!("\tNext: No other bags.");
                        }
                    }
                }
                true
            };
            !delete
        });
        //println!("Bags left {}", next_bags.len());
        colors.extend(next_bags);  
    }

    println!("{}", colors.len());
    println!("{}", total_count);
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}