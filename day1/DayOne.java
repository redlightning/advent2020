import java.io.File;  
import java.io.FileNotFoundException;  
import java.util.List;
import java.util.ArrayList;
import java.util.Scanner;

public class DayOne {
    private static List<Integer> expenses = new ArrayList<>();
  public static void main(String[] args) {
    try {
      File myObj = new File("input.txt");
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        expenses.add(Integer.parseInt(data));
      }
      myReader.close();
    } catch (FileNotFoundException e) {
      System.out.println("An error occurred.");
      e.printStackTrace();
    }

    find2();
    find3();
  }

  public static void find2() {
    for(int c = 0; c<expenses.size(); c++) {
      for(int x=0; x<expenses.size(); x++) {
        if(x != c) {
          if (expenses.get(c) + expenses.get(x) == 2020) {
            int mult = expenses.get(c) * expenses.get(x);
            System.out.println("TWO: " + expenses.get(c) + "*" + expenses.get(x) + "=" + mult);
            break;
          }
        }
      }
    }
  }

  public static void find3() {
    for(int x=0; x<expenses.size(); x++) {
      for(int y=1; y<expenses.size(); y++) {
        for(int z=2; z<expenses.size(); z++) {
          if(x!=y && x!=z && y!=z) {
            if (expenses.get(x) + expenses.get(y)  + expenses.get(z) == 2020) {
              int mult = expenses.get(x) * expenses.get(y) * expenses.get(z);
              System.out.println("THREE: " + expenses.get(x) + "*" + expenses.get(y) + "*" + expenses.get(z) + "=" + mult);
              break;
            }
          }
        }
      }
    }
  }
}
