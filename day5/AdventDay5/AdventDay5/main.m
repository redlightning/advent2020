#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        NSArray *samples = @[@"BFFFBBFRRR", @"FFFBBBFRRR", @"BBFFBBFRLL"];
        //NSMutableArray *rows = @[@"O",@"O",@"O",@"O",@"O",@"O",@"O",@"O"];
        NSString *path = @"/Users/misaacso/git/advent2020/day5/input.txt";
        NSError *error;
        NSString *fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
        if (error)
            NSLog(@"Error reading file: %@", error.localizedDescription);

        NSArray *lines  = [fileContents componentsSeparatedByString:@"\n"];
        NSLog(@"Lines: %d", (int)lines.count);
        
        NSMutableArray *seats = [[NSMutableArray alloc] init];
        
        for(int r=0; r<=127; r++) {
            [seats addObject: [NSMutableArray arrayWithObjects: @"O",@"O",@"O",@"O",@"O",@"O",@"O",@"O",nil]];
        }
        
        int highestId = 0;
        
        for (NSString *pattern in lines) {
            float rowStart = 0;
            float rowEnd = 127;
            float colStart = 0;
            float colEnd = 7;
            
            if([pattern length] > 7) {
                NSLog(@"%@", pattern);
                //Find Row
                for(int i=0; i< 7; i++) {
                    NSString *theCharacter = [NSString stringWithFormat:@"%c", [pattern characterAtIndex:i]];
                    if ([theCharacter isEqualToString: @"F"]) {
                        rowEnd = rowEnd - ((rowEnd-rowStart) / 2);
                        NSLog(@"\tR: %d..%d", (int)rowStart, (int)rowEnd);
                    } else if ([theCharacter isEqualToString: @"B"]) {
                        rowStart = rowStart + ((rowEnd-rowStart) / 2);
                        NSLog(@"\tR: %d..%d", (int)rowStart, (int)rowEnd);
                    }
                }
                
                //Find Column
                for(int c=7; c<10; c++) {
                    NSString *theCharacter = [NSString stringWithFormat:@"%c", [pattern characterAtIndex:c]];
                    if ([theCharacter isEqualToString: @"L"]) {
                        colEnd = colEnd - ((colEnd-colStart) / 2);
                        NSLog(@"\tC: %d..%d", (int)colStart, (int)colEnd);
                    } else if ([theCharacter isEqualToString: @"R"]) {
                        colStart = colStart + ((colEnd-colStart) / 2);
                        NSLog(@"\tC: %d..%d", (int)colStart, (int)colEnd);
                    }
                }
                
                NSLog(@"%@ - > R:%d C:%d", pattern, (int)rowEnd, (int)colEnd);
                
                NSMutableArray *temp = seats[(int)rowEnd];
                [temp replaceObjectAtIndex: (int)colEnd withObject: @"X"];
                
                int newId = ((int)rowEnd * 8) + (int)colEnd;
                if(newId > highestId) {
                    highestId = newId;
                }
            }
        }
        NSLog(@"%d", highestId);
         
        for(int d=0; d<=127; d++) {
            if([seats[d] containsObject:@"O"] && [seats[d] containsObject:@"X"]) {
                for(int y=0; y<8; y++) {
                    NSLog(@"%d %d %@",d ,y, seats[d][y]);
                    //Read output and get the result.
                }
            }
        }
    }
    return 0;
}
