import std.stdio;
import std.algorithm;
import std.range;
import std.string;
import std.conv;
import std.math;

int[] tribonacci(int i) 
{
    int[] tribonacciNumbers = [1,1,2];
    int index = 0;
    while (index < i) {
        const int[] the_sum  = [sum(tribonacciNumbers[index..$])];
        tribonacciNumbers = tribonacciNumbers ~ the_sum;
        index += 1;
    }
    return tribonacciNumbers[i];
}

void main() 
{
    File file = File("sample.txt", "r");
    
    int[] array;
    array.length = 110;
    int i;
    int max = 0;

    while (!file.eof()) 
    {
        string line = chomp(file.readln());
        if (i == array.length)
            array.length += 50;
        
        if (line.length != 0) 
        {
            array[i] = to!int(line);
            if (max < to!int(line)){
                max = to!int(line);
            }
            i++;
            writeln(line);
        }
    }
    array.length = i+2;
    file.close();
    array[i+1] = 0;
    array[i+2] = max;

    sort(array);
    
    // differences = [array[a+1]-array[a] for a in range(len(array)-1)];
    // diffString = ''.join([str(num) for num in differences]);
    // diffStrings = diffString.split('3');

    // print(prod([tribonacci(len(string)) for string in diffStrings]));
    // // Gets the right answer for both test inputs, but not the real input

    int POW2 = 0;
    int POW7 = 0;
    for (int x = 1; x < array.length - 1; x++)
    {
        long negative3 = (x >= 3) ? array[x - 3] : -9999;
        if (array[x + 1] - negative3 == 4)
        {
            POW7 += 1;
            POW2 -= 2;
        }
        else if (array[x + 1] - array[x - 1] == 2)
        {
            POW2 += 1;
        }
    }
    double danswer = pow(2,POW2) * pow(7, POW7);

    writeln(danswer);
}

// void part2()
// {
//     XMAS = (from yy in (from y in Input.Split('\n') select long.Parse(y)) orderby yy select yy).ToList();
//     //time to find out many can be omitted.
//     XMAS.Insert(0, 0);
//     XMAS.Add(XMAS.Last() + 3);
//     int POW2 = 0;
//     int POW7 = 0;
//     for (int i = 1; i < XMAS.Count-1; i++)
//     {
//         long negative3 = (i >= 3) ? XMAS[i - 3] : -9999;
//         if (XMAS[i + 1] - negative3 == 4)
//         {
//             POW7 += 1;
//             POW2 -= 2;
//         }
//         else if (XMAS[i + 1] - XMAS[i - 1] == 2)
//         {
//             POW2 += 1;
//         }
//     }
//     double danswer = System.Math.Pow(2,POW2) * System.Math.Pow(7, POW7);

//     return $"{danswer}";
// }
