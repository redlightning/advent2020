import Foundation

let completeUrl = URL(fileURLWithPath: "/Users/misaacso/git/advent2020/day6/input.txt" )

var inputData = ""

do {
 let fileData = try Data(contentsOf: completeUrl)
 if let savedString = String(data: fileData, encoding: .utf8) {
    inputData = savedString
 }
} catch {
 print("Unable to read the file")
}


var used = ""
var total = 0
var groupTotal = 0
let lines = inputData.components(separatedBy: "\n")

for line in lines {
    if line.isEmpty {
        //Reset
        print("Group Total: ", groupTotal)
        used = ""
        total = total + groupTotal
        groupTotal = 0
        
    } else {
        for answer in line {
            if !used.contains(answer) {
                used.append(answer)
                groupTotal = groupTotal + 1
            }
        }
    }
}
print("Total(Part 1) : ", total)

let alphabet = "abcdefghijklmnopqrstuvwxyz"
used = ""
total = 0
groupTotal = 0
var memberCount = 0
for line in lines {
    if line.isEmpty {
        for letter in alphabet {
            if used.filter({$0 == letter}).count == memberCount {
                groupTotal += 1
            }
        }
        //Reset
        print("Group Total: ", groupTotal)
        used = ""
        total = total + groupTotal
        groupTotal = 0
        memberCount = 0
        
    } else {
        memberCount += 1
        used.append(line)
    }
}

print("Total(Part 2): ", total)
