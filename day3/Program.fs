open System.IO


[<EntryPoint>]
let main argv =
    let filePath = @"input.txt"
    let lines = File.ReadLines(filePath)
    let mutable column = 0
    let mutable treeCount1 = 0L
    let mutable treeCount2 = 0L
    let mutable treeCount3 = 0L
    let mutable treeCount4 = 0L
    let mutable treeCount5 = 0L
    
    //R1D1
    for row in lines do
        if String.length(row) > 0 then
            if '#'.Equals(row.[column]) then
                treeCount1 <- treeCount1 + 1L
        
            column <- column + 1

            if String.length(row) <= column then
                column <- column - String.length(row)

    printfn "%d" treeCount1
    column <- 0

    // R3D1
    for row in lines do
        if String.length(row) > 0 then
            if '#'.Equals(row.[column]) then
                treeCount2 <- treeCount2 + 1L
        
            column <- column + 3

            if String.length(row) <= column then
                column <- column - String.length(row)

    printfn "%d" treeCount2
    column <- 0
    
    // R5D1
    for row in lines do
        if String.length(row) > 0 then
            if '#'.Equals(row.[column]) then
                treeCount3 <- treeCount3 + 1L
        
            column <- column + 5

            if String.length(row) <= column then
                column <- column - String.length(row)

    printfn "%d" treeCount3
    column <- 0

    // R7D1
    for row in lines do
        if String.length(row) > 0 then
            if '#'.Equals(row.[column]) then
                treeCount4 <- treeCount4 + 1L
        
            column <- column + 7

            if String.length(row) <= column then
                column <- column - String.length(row)

    printfn "%d" treeCount4
    column <- 0

    // R1D2
    let mutable rowCount = 0
    for row in lines do
        if String.length(row) > 0 && rowCount % 2 = 0 then
            if '#'.Equals(row.[column]) then
                treeCount5 <- treeCount5 + 1L
        
            column <- column + 1

            if String.length(row) <= column then
                column <- column - String.length(row)
        
        if String.length(row) > 0 then
            rowCount <- rowCount + 1

    printfn "%d" treeCount5

    let total : int64 = treeCount1 * treeCount2 * treeCount3 * treeCount4 * treeCount5
    printfn "%d" total

    0
