# Advent of Code 2020

http://adventofcode.com

My solutions for 2020's Advent of Code. I am going to try as many languages as possible this year, assuming I have time.

Day 1  - Java  
Day 2  - Ruby  
Day 3  - F#
Day 4  - Perl
Day 5  - Objective C
Day 6  - Swift
Day 7  - Rust
Day 8  - Go
Day 9  - Kotlin
Day 10 - D
Day 11 - 
Day 12 - 
Day 13 - 
Day 14 - 
Day 15 - C#
Day 16 - 
Day 17 - 
Day 18 - 
Day 19 - 
Day 20 - 
Day 21 - 
Day 22 - 
Day 23 - 
Day 24 - 
Day 25 - 